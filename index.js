// console.log("Hello World!");

/* 
//fetch()

//this is a method in JavaScript that is used to send request in the server and load the information(response from the server) in the webpages. (JSON format)

// SYNTAX
fetch("urlAPI", {options})
// url - the url which the request is to be made.
options - an array of properties, optional parameter.
 */

/* 
Endpoint of the API:

		GET and POST:
		https://sheltered-hamlet-48007.herokuapp.com/movies

		UPDATE and DELETE:
		https://sheltered-hamlet-48007.herokuapp.com/movies/:id

Get all data.
*/
let movies;

let fetchMovies = () => {
    fetch("https://sheltered-hamlet-48007.herokuapp.com/movies")
    .then(response => response.json())
    .then(data => showPosts(data));
}

fetchMovies();

const showPosts = (movies) => {
    console.log(movies);
    let movieEntries = "";
    //forEach() to loop
    movies.forEach(movie => {
        // We can assign HTML elements in JS Variables
        movieEntries += `
            <div id="movie-${movie._id}">
                <h3 id="movie-title-${movie._id}">${movie.title}</h3>
                <p id="movie-desc-${movie._id}">${movie.description}</p>
                <button onclick="editMovie('${movie._id}')">Edit</button>
                <button onclick="deleteMovie('${movie._id}')">Delete</button>
            </div>
        `;
    });

    //console.log(movieEntries);
    //To display the movies in our HTML document
    document.querySelector("#div-movie-entries")
    .innerHTML = movieEntries;
}


// Add Movie data
// We select the Add form using the query selector
// We listen with the submit button for events.
document.querySelector("#form-add-movie").addEventListener("submit", (e) => {

    // Prevents the page from loading
    e.preventDefault();

    let title = document.querySelector("#txt-title").value
    let description = document.querySelector("#txt-desc").value

    fetch("https://sheltered-hamlet-48007.herokuapp.com/movies", {
        method: "POST",
        body: JSON.stringify({
            title: title,
            description: description
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert("Succesfully added.");
            // querySelector = null, resets the state of our input into blanks after submitting a new post
            document.querySelector("#txt-title").value = null;
            document.querySelector("#txt-desc").value = null;

            fetchMovies();
        })
})




// Update a movie
// This will trigger an event that will update a certain movie upon clicking the update button.

document.querySelector("#form-edit-movie").addEventListener("submit", (e) => {

    e.preventDefault();

    let id = document.querySelector("#txt-edit-id").value;

    fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`, {
        method: "PUT",
        body: JSON.stringify({
            title: document.querySelector("#txt-edit-title").value,
            description: document.querySelector("#txt-edit-desc").value
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert("Succesfully updated.");

            document.querySelector("#txt-edit-id").value = null;
            document.querySelector("#txt-edit-title").value = null;
            document.querySelector("#txt-edit-desc").value = null;
            document.querySelector("#btn-submit-update").setAttribute("disabled", true);

            // show updates
            fetchMovies();
        });
});




// Edit Movie Button
// Create the JS function called in the onclick event.
const editMovie = (id) => {

    console.log(id);
    let title = document.querySelector(`#movie-title-${id}`).innerHTML;
    let description = document.querySelector(`#movie-desc-${id}`).innerHTML;

    console.log(title);
    console.log(description);

    // Pass the id, title, and description in the Edit form and enable the Update button
    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-desc").value = description;
    document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

// Delete a movie
// This will trigger an event that will update a certain movie upon clicking the update button.

const deleteMovie = (id) => {

 console.log(id);
    const element = document.getElementById(`movie-${id}`);
    
   element.remove();
    fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`, {
        method: 'DELETE',
    })
        .then(res => res.text()) // or res.json()
        .then(res => console.log(res))
}

/*
    1. Add a new function in index.js called deleteMovie().
    2. This function should be able to receive the id number of the post:
        - Create a fetch request to delete a post document by its id.
    3. Create a git repository named S49.
    4. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
   5.  Add the link in Boodle.
*/
